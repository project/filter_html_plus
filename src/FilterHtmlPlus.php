<?php

namespace Drupal\filter_html_plus;

use Drupal\filter\Plugin\Filter\FilterHtml;

/**
 * Return response after filtration.
 */
class FilterHtmlPlus extends FilterHtml {

  /**
   * {@inheritdoc}
   */
  public function getHtmlRestrictions() {
    if ($this->restrictions) {
      return $this->restrictions;
    }
    // Libxml 2.9.14 (or maybe 2.9.13) no longer supports treating core's star
    // protector (__zqh6vxfbk3cg__) as a valid tag due to the leading
    // underscores, so we have to use our own.  The old code worked fine on
    // libxml 2.9.12 but not on 2.9.14
    $star_protector = 'filterhtmlplusstar';
    $this->settings['allowed_html'] = str_replace('<*', '<'. $star_protector, $this->settings['allowed_html']);
    $restrictions = parent::getHtmlRestrictions();
    if (isset($restrictions['allowed'][$star_protector])) {
      $restrictions['allowed']['*'] = array_merge($restrictions['allowed']['*'], $restrictions['allowed'][$star_protector]);
      unset($restrictions['allowed'][$star_protector]);
    }

    $this->restrictions = $restrictions;

    return $this->restrictions;
  }

}
