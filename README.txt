CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module does one simple thing: it allows you to whitelist attributes
globally, for all HTML tags, when using the core "Limit allowed HTML tags and
correct faulty HTML" text filter.

More info:

 * For a full description of the module, visit [the project page]
   (https://www.drupal.org/project/filter_html_plus).

REQUIREMENTS
------------

This module requires only the filter module that's part of Drupal core.


INSTALLATION
------------

 * Install the Filter HTML Plus module as you would normally install a 
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 
   for further information.

CONFIGURATION
-------------

 * In the "Allowed HTML Tags" configuration for any input formats using core's
   "Limit allowed HTML tags and correct faulty HTML" filter, configure attributes that are
   permissable for all tags by using the * wildcard.  For example, <* class tabindex> will
   allow the use of class and tabindex attributes on all tags.


MAINTAINERS
-----------

Current maintainers:

 * Aaron Zinck (azinck) - https://www.drupal.org/u/azinck
